const express = require("express")
const data = require("./account.json");
const app = express()

app.use(express.static('public'))

app.get("/data",(req,res)=>{
    res.status(200).send(data);
});

app.listen(8080);

module.exports = app;