function applyDiscount(vehicles, discount){
    return new Promise((res,rej)=>{
        if(typeof discount === "number"){
            let ok = true;
            let max = 99999999999;
            
            if (vehicles.find(vehicle => typeof vehicle.make !=="string" || typeof vehicle.price !== "number")){
                rej(new Error("Invalid array format"));
            }
            
            const min = Math.min(...vehicles.map(vehicle => vehicle.price));
            if(discount > (0.5*min)){
                rej(new Error("Discount too big"));
            }
            
            res(vehicles.map(vehicle => {
                return {make: vehicle.make, price: vehicle.price-discount};
            }));
            
        }else{
            rej(new Error("Invalid discount"));
        }
    })
    
    //ALTERNATIVA
    // if (vehicles.find(typeof vehicle.make !=="string" || typeof vehicle.price !== "number"))
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;